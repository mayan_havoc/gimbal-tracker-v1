import CustomLayout from "./wrapPageElement";
import "@fontsource/heebo";
import "@fontsource/hammersmith-one";
import "@fontsource/archivo-black";
import "@fontsource/rubik";

export const wrapPageElement = CustomLayout;