import Cardano from '../serialization-lib'
import {
    assetsToValue,
    createTxOutput,
    finalizeTx,
    initializeTx,
    createTxUnspentOutput,
} from "../transaction";
import { fromBech32, fromHex } from "../../utils/converter";
import { treasuryContractAddress, treasuryIssuerAddress, bountyTokenUnit } from '../treasury-contract';
import { serializeTreasuryDatum } from '../treasury-contract/datums';
import { createTreasuryDatum, getAddressKeyHash } from '../../utils/factory';
import { contractScripts } from '../treasury-contract/validator';

// basic locking-tx for INITIALLY FUNDING a TREASURY
export const lockingTx = async ({ address, utxosParam, lovelace, gimbals }) => {
    try {
        const { txBuilder, outputs } = initializeTx();
        const utxos = utxosParam.map((utxo) =>
            Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
        );

        const tDatum = createTreasuryDatum("101", treasuryIssuerAddress)
        const treasuryDatum = serializeTreasuryDatum(tDatum)

        console.log("Sending to Treasury:")
        console.log("Lovelace: ", lovelace)
        console.log("Gimbals: ", gimbals)

        outputs.add(
            createTxOutput(
                Cardano.Instance.Address.from_bech32(treasuryContractAddress),
                assetsToValue([
                    { unit: "lovelace", quantity: `${lovelace}` },
                    { unit: `${bountyTokenUnit}`, quantity: `${gimbals}` }
                ]),
                { datum: treasuryDatum }
            )
        )
        // logging
        console.log("txBuilder", txBuilder)

        const txHash = await finalizeTx({
            txBuilder,
            utxos,
            outputs,
            changeAddress: fromBech32(address),
        })

        return {
            txHash,
        };
    }
    catch (error) {
        console.log(error, "in lockingTx")
    }


};


// another locking-tx for ADDING FUNDS to an EXISTING TREASURY
export const addFundsToTreasuryTx = async ({ address, utxosParam, lovelaceToAdd, gimbalsToAdd, tUtxo, lovelaceAtTreasury, gimbalsAtTreasury }) => {
    try {
        const { txBuilder, datums, outputs } = initializeTx();
        const utxos = utxosParam.map((utxo) =>
            Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
        );

        const lovelaceOut = parseInt(lovelaceToAdd) + parseInt(lovelaceAtTreasury)
        const gimbalsOut = parseInt(gimbalsToAdd) + parseInt(gimbalsAtTreasury)

        console.log("New Treasury UTXO will contain:")
        console.log("Lovelace: ", lovelaceOut)
        console.log("Gimbals: ", gimbalsOut)

        // Create and serialize "dummy datum":
        const tDatum = createTreasuryDatum("101", treasuryIssuerAddress)
        const treasuryDatum = serializeTreasuryDatum(tDatum)
        // In this implementation, the treasuryDatum does not change when the Issuer adds funds to Treasury
        // That's why we can use datums.add(treasuryDatum) for unlocking the existing Treasury UTXO...
        datums.add(treasuryDatum)

        // ...and then include the same treasuryDatum on the "new" output:
        outputs.add(
            createTxOutput(
                Cardano.Instance.Address.from_bech32(treasuryContractAddress),
                assetsToValue([
                    { unit: "lovelace", quantity: `${lovelaceOut}` },
                    { unit: `${bountyTokenUnit}`, quantity: `${gimbalsOut}` }
                ]),
                { datum: treasuryDatum }
            )
        )

        const treasuryUtxo = createTxUnspentOutput(Cardano.Instance.Address.from_bech32(treasuryContractAddress), tUtxo)

        // To use the Issuer's "back door" to managing Treasury, we need the Issuer as Required Signer
        const iAddress = fromBech32(treasuryIssuerAddress)
        const requiredSigners = Cardano.Instance.Ed25519KeyHashes.new();
        requiredSigners.add(iAddress.payment_cred().to_keyhash());
        txBuilder.set_required_signers(requiredSigners);

        // Temporary (2022-05-05) a "Dummy Redeemer" to use until we add Treasury Actions to Treasury Contract
        const dummyRedeemer = {
            issuer: getAddressKeyHash(treasuryIssuerAddress),
            contributor: getAddressKeyHash(treasuryIssuerAddress),
            lovelaceAmount: 0,
            tokenAmount: 0,
            expirationTime: "1000000000"
        }

        // just logging
        console.log("txBuilder", txBuilder)

        const txHash = await finalizeTx({
            txBuilder,
            datums,
            utxos,
            outputs,
            changeAddress: fromBech32(address),
            scriptUtxo: treasuryUtxo,
            bountyInfo: dummyRedeemer,
            plutusScripts: contractScripts(),
        })

        return {
            txHash,
        };
    }
    catch (error) {
        console.log(error, "in addFundsToTreasuryTx")
    }


};