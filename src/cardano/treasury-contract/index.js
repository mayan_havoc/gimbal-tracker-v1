import Cardano from "../serialization-lib";
import { deserializeBounty, serializeBountyDatum, serializeTreasuryDatum } from "./datums";
import {
  assetsToValue,
  createTxOutput,
  createTxUnspentOutput,
  finalizeTx,
  initializeTx,
} from "../transaction";
import { fromBech32, fromHex, toHex } from "../../utils/converter";
import { contractScripts } from "./validator";
import { getAddressKeyHash } from "../../utils/factory";

// Project Instance Variables:
export const treasuryIssuerAddress = "addr_test1qz2h42hnke3hf8n05m2hzdaamup6edfqvvs2snqhmufv0eryqhtfq6cfwktmrdw79n2smpdd8n244z8x9f3267g8cz6s59993r"
export const treasuryContractAddress = "addr_test1wry45ukylk3vcd9hdnt9xc5v4pplu8e4va2rw0fqvc6f22c0e6z4h"
export const bountyContractAddress = "addr_test1wrys8gqws6v4qppt3xcz305ejs2pztmuxdvafcaf7mu48jggf3v30"
export const accessPolicyID = "5d6b6c332866044b2a8bdd147d92f77e42714986f1cb98cef70e201f"
export const bountyTokenUnit = "6c57132fde399c9ea6e462c4214d164984891087922b6fa2472b175b7470626c5465737447696d62616c"

export const commitToBounty = async (bDatum, tDatum, { contributorAddress, utxosParam, bountySlug, accessTokenName, bLovelace, bGimbals, tUtxo, tLovelaceIn, tGimbalsIn }) => {
  try {
    const { txBuilder, datums, outputs } = initializeTx();
    const utxos = utxosParam.map((utxo) =>
      Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
    );

    console.log("your utxos", utxos)

    // Convert Access Token Name to Hex
    const accessTokenHex = toHex(accessTokenName)
    console.log("Access Token:", accessTokenName, accessTokenHex)


    // do some quick arithmetic: the Treasury Contract output should contain all of the Lovelace and Gimbals that are NOT sent to the bounty.
    // (In order to unlock funds from the Treasury, The Treasury Validator will insist on meeting this condition.)
    const tLI = parseInt(tLovelaceIn)
    const tGI = parseInt(tGimbalsIn)
    const lovelaceToTreasury = tLI - bLovelace
    const gimbalsToTreasury = tGI - bGimbals

    console.log("bounty bound:", bLovelace, bGimbals, lovelaceToTreasury, gimbalsToTreasury)
    console.log(bountyContractAddress)

    const bountyDatum = serializeBountyDatum(bDatum);
    console.log("serialized bountyDatum", bountyDatum);
    console.log("and d", deserializeBounty(bountyDatum))

    const treasuryDatum = serializeTreasuryDatum(tDatum);
    datums.add(treasuryDatum);

    const dataForRedeemer = {
      issuer: getAddressKeyHash(treasuryIssuerAddress),
      contributor: getAddressKeyHash(contributorAddress),
      lovelaceAmount: `${bLovelace}`,
      tokenAmount: `${bGimbals}`,
      expirationTime: "1000000000"
    }

    console.log("dataForRedeemer", dataForRedeemer)

    // 4-17 -- why are inputs exhausted?

    // Output to Bounty Contract

    // TODO: The Unit string for the Access token must by dynamic
    outputs.add(
      createTxOutput(
        Cardano.Instance.Address.from_bech32(bountyContractAddress),
        assetsToValue([
          { unit: "lovelace", quantity: `${bLovelace}` },
          { unit: `${bountyTokenUnit}`, quantity: `${bGimbals}` },
          { unit: `${accessPolicyID}${accessTokenHex}`, quantity: "1"}
        ]),
        { datum: bountyDatum }
      )
    );

    outputs.add(
      createTxOutput(
        Cardano.Instance.Address.from_bech32(treasuryContractAddress),
        assetsToValue([
          { unit: "lovelace", quantity: `${lovelaceToTreasury}` },
          { unit: `${bountyTokenUnit}`, quantity: `${gimbalsToTreasury}` }
        ]),
        { datum: treasuryDatum }
      )
    );

    const treasuryUtxo = createTxUnspentOutput(Cardano.Instance.Address.from_bech32(treasuryContractAddress), tUtxo)

    const txHash = await finalizeTx({
      txBuilder,
      datums,
      utxos,
      outputs,
      changeAddress: fromBech32(contributorAddress),
      metadata: bountySlug,
      scriptUtxo: treasuryUtxo,
      bountyInfo: dataForRedeemer, // just changed
      plutusScripts: contractScripts(),
    });
    return {
      txHash,
    };
  } catch (error) {
    console.log(error, "commitToBounty");
    return {
      error
    }
  }
};