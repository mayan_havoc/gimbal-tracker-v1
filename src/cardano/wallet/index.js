import Cardano from "../serialization-lib";
import { fromHex } from "../../utils/converter";
import { serializeTxUnspentOutput, valueToAssets } from "../transaction";
import getWalletDict from "./walletDict";

class Wallet {
  async enable(name) {
    const walletDict = await getWalletDict()
    const wallet = (walletDict)[name];

    if (wallet) {
      const instance = await wallet.enable();
      if (instance) {
        this._provider = instance;
        // temporary fix - remove below when Nami fixes itself
        if (name === "Nami") {
          this._provider = window.cardano;
        }
        return true;
      }
    }
    return false;
  }

  async load() {
    if (window.cardano === undefined) return;

    await Cardano.load();

    return this._provider;
  }


  async getBalance() {
    const balance = await this._provider.getBalance();

    return Cardano.Instance.Value.from_bytes(fromHex(balance)).coin().to_str();
  }


  async getCollateral() {
    return await this._provider.getCollateral();
  };

  async getNetworkId() {
    return await this._provider.getNetworkId();
  };

  async getUsedAddresses() {
    const usedAddresses = await this._provider.getUsedAddresses();

    return usedAddresses.map((address) =>
      Cardano.Instance.Address.from_bytes(fromHex(address)).to_bech32()
    );
  };

  async getUtxos() {
    return await this._provider.getUtxos();
  };

  async getAssets() {
    const utxos = await this._provider.getUtxos();

    const nativeAssets = utxos
      .map((utxo) => serializeTxUnspentOutput(utxo).output())
      .filter((txOut) => txOut.amount().multiasset() !== undefined)
      .map((txOut) => valueToAssets(txOut.amount()))
      .flatMap((assets) =>
        assets
          .filter((asset) => asset.unit !== "lovelace")
          .map((asset) => asset.unit)
      );

    return [...new Set(nativeAssets)];
  }


  async signTx(tx, partialSign = true) {
    return await this._provider.signTx(tx, partialSign);
  };

  async submitTx(tx) {
    return await this._provider.submitTx(tx);
  };
}

export default new Wallet();
