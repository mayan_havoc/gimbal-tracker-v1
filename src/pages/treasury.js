import React, { useEffect, useState } from "react"
import { useStoreState } from "easy-peasy"
import {
    Flex, Heading, Text, Box, Button,
    Input, FormControl, FormLabel, FormErrorMessage, FormHelperText,
    useDisclosure, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton,
} from "@chakra-ui/react"
import { Formik, useFormik } from 'formik';
import { useUtxosFromAddress } from "../hooks/useUtxosFromAddress"
import useWallet from "../hooks/useWallet"
import { treasuryContractAddress, treasuryIssuerAddress, bountyTokenUnit } from "../cardano/treasury-contract"
import CommittedBounty from "../components/CommittedBounty"
import { addFundsToTreasuryTx, lockingTx } from "../cardano/locking-tx"
import { getAddressKeyHash } from "../utils/factory"

const Treasury = () => {

    // Get connected wallet from Easy Peasy
    const { wallet } = useWallet(null)
    const connected = useStoreState((state) => state.connection.connected);
    const [walletUtxos, setWalletUtxos] = useState([]);

    const [treasuryLovelace, setTreasuryLovelace] = useState(0)
    const [treasuryGimbals, setTreasuryGimbals] = useState(0)
    const [numTreasuryUtxos, setNumTreasuryUtxos] = useState(0)
    const [txHashAtTreasury, setTxHashAtTreasury] = useState(null)
    const [txIxAtTreasury, setTxIxAtTreasury] = useState("0")

    // For Success notification:
    const [successfulTxHash, setSuccessfulTxHash] = useState(null)

    // Initialize Modals
    const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure()
    const { isOpen: isErrorOpen, onOpen: onErrorOpen, onClose: onErrorClose } = useDisclosure()

    // Use this hook to query the Bounty Contract address
    const {
        utxos: treasuryUtxos,
        getUtxos,
        loading: utxosLoading,
        error,
    } = useUtxosFromAddress();

    useEffect(async () => {
        if (connected && wallet) {
            await getUtxos({
                variables: {
                    addr: treasuryContractAddress,
                },
            });
            const myUtxos = await wallet.utxos;

            setWalletUtxos(myUtxos);
        }
    }, [wallet])

    useEffect(() => {
        if(treasuryUtxos?.utxos.length > 0){
            setNumTreasuryUtxos(treasuryUtxos?.utxos.length)
            setTreasuryLovelace(treasuryUtxos?.utxos[0]?.value)
            setTreasuryGimbals(treasuryUtxos?.utxos[0]?.tokens[0]?.quantity)
            setTxHashAtTreasury(treasuryUtxos?.utxos[0]?.transaction.hash)
            setTxIxAtTreasury(treasuryUtxos?.utxos[0]?.index)
        }
    }, [treasuryUtxos])

    const formik = useFormik({
        initialValues: {
            lovelaceToLock: 0,
            gimbalsToLock: 0,
        },
    })

    const [lovelaceToLock, setLovelaceToLock] = useState(formik.lovelaceToLock)
    const [gimbalsToLock, setGimbalsToLock] = useState(formik.gimbalsToLock)

    useEffect(() => {
        const n = formik.values.lovelaceToLock;
        setLovelaceToLock(n);
    }, [formik.values.lovelaceToLock])

    useEffect(() => {
        const n = formik.values.gimbalsToLock;
        setGimbalsToLock(n);
    }, [formik.values.gimbalsToLock])

    const handleLockFundsInTreasury = async () => {
        try {
            const treasuryLockParams = {
                address: connected,
                utxosParam: walletUtxos,
                lovelace: lovelaceToLock,
                gimbals: gimbalsToLock
            }
            const successTxHash = await lockingTx(treasuryLockParams)
            if (successTxHash.error) throw "error creating tx"
            else {
                setSuccessfulTxHash(successTxHash);
                onSuccessOpen();
            }
        } catch (error) {
            console.log("Error locking Treasury Funds", error)
            onErrorOpen();
        }
    }

    const handleAddFundsToTreasury = async () => {
        try {
            const tUtxo = {
                "tx_hash": txHashAtTreasury,
                "output_index": txIxAtTreasury,
                "amount": [
                    { "unit": "lovelace", "quantity": `${treasuryLovelace}` },
                    { "unit": `${bountyTokenUnit}`, "quantity": `${treasuryGimbals}` }
                ],
            }

            const treasuryDepositParams = {
                address: connected,
                utxosParam: walletUtxos,
                lovelaceToAdd: lovelaceToLock,
                gimbalsToAdd: gimbalsToLock,
                tUtxo,
                lovelaceAtTreasury: treasuryLovelace,
                gimbalsAtTreasury: treasuryGimbals
            }
            const successTxHash = await addFundsToTreasuryTx(treasuryDepositParams)
            if (successTxHash.error) throw "error creating tx"
            else {
                setSuccessfulTxHash(successTxHash);
                onSuccessOpen();
            }
        } catch (error) {
            console.log("Error adding funds to Treasury", error)
            onErrorOpen();
        }
    }



    return (
        <>
            <title>TREASURY</title>
            <Flex
                w="100%"
                mx="auto"
                direction="column"
                wrap="wrap"
                bg="gl-yellow"
                p="10"
            >
                <Box w="70%" mx="auto" my="5">
                    <Heading py='5'>Ada and Native Assets in Treasury</Heading>
                    <Text py='3'>Treasury Issuer Address: {treasuryIssuerAddress}</Text>
                    <Text p='2'>Ada: {treasuryLovelace / 1000000}</Text>
                    <Text p='2'>Gimbals: {treasuryGimbals}</Text>
                    <Text p='2'>
                        # UTXOs at Treasury Contract: {numTreasuryUtxos}
                    </Text>
                    {connected == treasuryIssuerAddress ? (
                        <Box bg='blue.200' m='5' p='5'>
                            <Heading py='1'>Issuer can Lock Funds in Treasury</Heading>
                            <Text p='1'>
                                You are the Bounty Issuer specified for this Treasury, so you can lock funds in the Treasury.
                            </Text>
                            <FormControl>
                                <FormLabel># Lovelace to Lock:</FormLabel>
                                <Input name="lovelaceToLock" onChange={formik.handleChange} value={formik.values.lovelaceToLock} />
                                <FormHelperText>in lovelace</FormHelperText>
                                <FormLabel># tGimbals to Lock</FormLabel>
                                <Input name="gimbalsToLock" onChange={formik.handleChange} value={formik.values.gimbalsToLock} />
                                <FormHelperText>no decimal points on testnet</FormHelperText>
                            </FormControl>
                            <Text p='1'>
                                Ready to {numTreasuryUtxos > 0 ? "add:" : "initialize Treasury with:"}
                            </Text>
                            <Text p='1'>
                                {lovelaceToLock} lovelace
                            </Text>
                            <Text p='1'>
                                {gimbalsToLock} gimbals
                            </Text>

                            {numTreasuryUtxos > 0 ? (
                                <Button m='5' onClick={handleAddFundsToTreasury}>Add Tokens to Treasury</Button>
                            ) : (
                                <Button m='5' onClick={handleLockFundsInTreasury}>Initialize Treasury</Button>
                            )}

                        </Box>

                    ) : (
                        <Box bg='blackAlpha.900' m='5' p='5' color='white'>You are not the Bounty Issuer for this Treasury, so there is nothing to see here.</Box>
                    )}
                </Box>
                <Modal isOpen={isSuccessOpen} onClose={onSuccessClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Tx Submitted</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>You successfully added funds to Treasury</Heading>
                            <Text py='2' fontSize='xs'>Tx Hash: {successfulTxHash?.txHash}</Text>
                        </ModalBody>
                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onSuccessClose}>
                                Let's Go!
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
                <Modal isOpen={isErrorOpen} onClose={onErrorClose}>
                    <ModalOverlay />
                    <ModalContent>
                        <ModalHeader>Tokens were not sent to Treasury</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <Heading size='sm'>There was an Error with this tx</Heading>
                            <Text>We are working on extending the error reporting for this Dapp (see bounty!). Please check the browser console to identify the error.</Text>
                        </ModalBody>
                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} onClick={onErrorClose}>
                                OK
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </Flex>
        </>
    )
}


export default Treasury
