---
slug: "i-btv1-007"
date: "2022-04-29"
title: "Improve project performance"
tags: ["Open", "Priority"]
scope: "this-project"
ada: 3
gimbals: 25000
---

## Outcome: Optimize dapp performance

## How to complete this bounty:
- On `/bounties`, `/bounties/slug`, and `/distribute`, how can we refactor React hooks?
- Are we using Easy Peasy's `store.js` effectively? Is there anything else that we should store in state?