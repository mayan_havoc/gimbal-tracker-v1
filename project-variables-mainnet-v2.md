# Mainnet Deployment Guide

## Git Branch: `mainnet-jd-v2`

## Project Variables:
- The Issuer's address is: `addr1qy3xh5h4gjt6z69jqvrt04l0g2d82h4s6cf0xvu7t2nz95j6jnltpt22t0myhsuk9s6lyn74yxmlxakt27ylr9ykh30qz97lf8`
- This issuers PKH is: `226bd2f54497a168b20306b7d7ef429a755eb0d612f3339e5aa622d2`
- The Bounty Payment token is: `gimbal`: `2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30.67696d62616c`
- In this instance, the Access Token PolicyID is: `68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67`
- Bounty Treasury Contract: `addr1w96xgwa3hhfu7mfazsaz8jkg0kf3fpk9x5z6r50y2vf23dq7aa2s5`
- Bounty Escrow Contract: `addr1wy48f7uvzrv7agqz2prdchg3dv3ksleqn4gnvckhsdmzjzgp6f497`
- Bounty Escrow Validator Hash: `2a74fb8c10d9eea0025046dc5d116b23687f209d513662d783762909`

## Here is a list of files where (for now), you'll need to tweak some variables:
- `gatsby-config.js`
- `components/WalletButton`
- `src/cardano/bounty-contract`
    - `plutus.js`
- `src/cardano/treasury-contract`
    - `plutus.js`
    - `index.js`
- `src/cardano/locking-tx`
- `src/cardano/transaction/index.js`

## Steps:
### 0. In `gatsby-config.js` change the Dandelion URL to match testnet or mainnet as needed.

### 1. In `/src/components/WalletButton/WalletButton.jsx`, look for
```
if ((await window.cardano.getNetworkId()) === 0) return true;
```
- Testnet -> `0` | Mainnet -> `1`

### 2. In `src/cardano/bounty-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-escrow-vX.plutus`
### 3. In `src/cardano/treasury-contract`:
- In `plutus.js`:
    - Replace the `cborHex` with the string from compiled `_-bounty-treasury-vX.plutus`
- in `index.js` (this is the big one!)
    - Replace `// Project Instance Variables` with the corresponding variables above

### 4. In `templates/bountyPage.js`:
- look for where frontmatter is called at top of file; toggle between testnet and mainnet (comment in/out) to handle Gimbal decimal places on Mainnet.

### 5 (optional). In `src/cardano/transaction/index.js`:
- The transaction metadata key is hard-coded into this file. For now, search for `1618`, and change it if you'd like.